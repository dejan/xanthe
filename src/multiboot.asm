; Multiboot bootloader for Xanthe
; https://www.gnu.org/software/grub/manual/multiboot/multiboot.html
;
; This is supported by the Grub bootloader, and by Qemu using the -kernel flag.  The Qemu support is really convenient because it
; doesn't require a disk image.  Unfortunately, Qemu doesn't support Multiboot 2, or setting graphics modes.
;
; I originally toyed with the idea of making multiple backends for Xanthe, but I dropped it.  This bootloader is designed to be
; completely compatible with the BIOS bootloader.  It feels kludgey, but it achieves that by dropping back into 16b real mode
; before booting back through protected mode like the BIOS bootloader does.

global start

section boot_header nowrite exec align=32

%define multiboot_magic 0x1badb002
%define multiboot_flags 0

header:
	dd multiboot_magic
	dd multiboot_flags
	dd 0x100000000 - (multiboot_magic + multiboot_flags)  ; checksum
header_end:

section .text
bits 32

extern systemEntry
extern stack_address
extern bss_start
extern bss_size

extern real_mode_text_load_start
extern real_mode_text_load_len
extern real_mode_text_vma

%include "common_defs.asm"
bits 32

start:
	mov esi, real_mode_text_load_start
	mov ecx, real_mode_text_load_len
	mov edi, real_mode_text_vma 
	rep movsb

	jmp low_mem_entry

section real_mode_text nowrite exec

%include "common.asm"

real_mode_return dw 0

bits 32
low_mem_entry:
	; Real mode and protected mode use different stacks, so instead of using a normal function call that puts the return address
	; onto the stack, this "function" expects the return address to be in a special variable.
	mov word [real_mode_return], real_boot
	jmp enter_real_mode

bits 16
real_boot:
	; Set legacy VGA graphics mode
	mov ax, 0x0013
	int 0x10

	cli

	; Enable protected mode
	mov eax, cr0
	or al, 1
	mov cr0, eax

	jmp seg_code32:.enter_32b
	.enter_32b: bits 32
	mov ax, seg_data
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	mov esp, stack_address

	; IRQ fix
	push dword 0x20
	call move_irqs
	add sp, 4
	; Mask all IRQs for now
	mov al, 0xff
	out 0x21, al
	out 0xa1, al

	; Grub seems to clear bss for us, but the Multiboot spec doesn't guarantee it, and other bootloaders don't do it
	mov edi, bss_start
	mov ecx, bss_size
	xor al, al
	rep stosb

	finit

	call systemEntry

	mov word [real_mode_return], shutdown
	jmp enter_real_mode

bits 32
enter_real_mode:
	push dword 0x00
	call move_irqs
	add sp, 4

	; Break back into 16b real mode
	cli
	lgdt [gdtr]
	jmp seg_code16:.enter_16b
	.enter_16b: bits 16
	mov eax, cr0
	and al, 0xfe
	mov cr0, eax
	jmp 0:.real_mode
	.real_mode:
	xor ax, ax
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov esp, 0x7000
	lidt [real_mode_idtr]
	sti

	jmp word [real_mode_return]
