; Descriptor Table Definitions
struc dt_entry
	dt_limit_lo: resb 2
	dt_base_lo: resb 3
	dt_access: resb 1
	dt_limit_hi: resb 1
	dt_base_hi: resb 1
endstruc
%define seg_code32 1 * dt_entry_size
%define seg_data 2 * dt_entry_size
%define seg_code16 3 * dt_entry_size
