module game.enemies;
@nogc:
nothrow:

import os.graphics;
import os.sound : Sound;
import os.system : Tick;

import game.environment;
import game.rigid_body;

struct Mine
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Mine;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			auto mine = cast(Mine*) mass;
			if (env.tick >= mine._reload_complete_tick)
			{
				auto orb_pos = mine.pos + Mine.kDim / 2 - Orb.kDim / 2;
				Orb.make(env, orb_pos);
				mine._reload_complete_tick = env.tick + kReloadDelay;
			}
			return mine.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			_sprite.render(screen, mass.pos);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Mine mine;
		mine.render(screen, 0);
	}

	enum kMaxNumInstances = 16;
	enum kZIndex = 3;
	enum kInitialV = Vector(0, 1);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 6;
	enum kDim = Vector(12, 12);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.Enemy;
	enum kCollisionDamage = 3;
	enum kExplosionSpeed = 8.0;
	enum kExplosionEnergy = 32;
	enum kScoreValue = 100;
	static immutable Sound[] explosion_sounds = [
		Sound.load!"mine_explosion1.raw",
		Sound.load!"mine_explosion2.raw",
	];

	private:

	enum kReloadDelay = 30;

	static immutable _sprite = PcxImage.load!"mine.pcx";

	Tick _reload_complete_tick;
}

struct Orb
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Orb;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			import std.math : sgn;
			auto target_vx = sgn(env.player.pos.x - mass.pos.x) * 0.5;
			auto target_ax = sgn(target_vx - mass.v.x) * 0.2;
			auto target_ay = (mass.v.y < 1.01) ? 0.2 : 0;
			mass.a = Vector(target_ax, target_ay);
			return mass.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			auto sprite_num = tick % _sprites.length;
			_sprites[sprite_num].render(screen, mass.pos);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Orb orb;
		foreach (tick; 0 .. 2 * _sprites.length)
		{
			orb.render(screen, 0);
		}
	}

	enum kMaxNumInstances = 6 * Mine.kMaxNumInstances;
	enum kZIndex = 4;
	enum kInitialV = Vector(0, 1);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 3;
	enum kDim = Vector(3, 3);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.Enemy;
	enum kCollisionDamage = 1;
	enum kExplosionSpeed = 4.0;
	enum kExplosionEnergy = 3;
	enum kScoreValue = 1;
	static immutable Sound[] explosion_sounds = [];

	private:

	static immutable _sprites = [
		PcxImage.load!"orb/1.pcx",
		PcxImage.load!"orb/2.pcx",
		PcxImage.load!"orb/3.pcx",
		PcxImage.load!"orb/4.pcx",
		PcxImage.load!"orb/5.pcx",
	];
}

struct Cannon
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Cannon;

	void setup(Environment* env) pure
	{
		_reload_complete_tick = env.tick + env.getRandom(kReloadDelay);
	}

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			auto cannon = cast(Cannon*) mass;
			if (env.tick >= cannon._reload_complete_tick)
			{
				auto bolt_pos = cannon.pos + kBoltDelta;
				CannonBolt.make(env, bolt_pos);
				cannon._reload_complete_tick = env.tick + kReloadDelay;
			}
			return cannon.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			_sprite.render(screen, mass.pos);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Cannon cannon;
		cannon.render(screen, 0);
	}

	enum kMaxNumInstances = 16;
	enum kZIndex = 3;
	enum kInitialV = Vector(0, 1);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 20;
	enum kDim = Vector(17, 18);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.Enemy;
	enum kCollisionDamage = 5;
	enum kExplosionSpeed = 8.0;
	enum kExplosionEnergy = 32;
	enum kScoreValue = 500;
	static immutable Sound[] explosion_sounds = [
		Sound.load!"cannon_explosion1.raw",
		Sound.load!"cannon_explosion2.raw",
	];

	private:

	enum kReloadDelay = 60;
	enum kBoltDelta = Cannon.kDim / Vector(2, 1) - Vector(CannonBolt.kDim.x / 2, 0);

	static immutable _sprite = PcxImage.load!"cannon.pcx";

	Tick _reload_complete_tick;
}

struct CannonBolt
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!CannonBolt;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			return mass.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			_sprite.render(screen, mass.pos);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		CannonBolt cannon_bolt;
		cannon_bolt.render(screen, 0);
	}

	enum kMaxNumInstances = 2 * Cannon.kMaxNumInstances;
	enum kZIndex = 4;
	enum kInitialV = Vector(0, 1);
	enum kInitialA = Vector(0, 1);
	enum kInitialHealth = 3;
	enum kDim = Vector(3, 7);
	enum kDrag = 0.1;
	enum kTeam = RigidBody.Team.Enemy;
	enum kCollisionDamage = 3;
	enum kExplosionSpeed = 8.0;
	enum kExplosionEnergy = 16;
	enum kScoreValue = 100;
	static immutable Sound[] explosion_sounds = [];

	private:
	static immutable _sprite = PcxImage.load!"cannon_bolt.pcx";
}

struct Meteoroid
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Meteoroid;

	void setup(Environment* env) pure
	{
		_sprite_num = env.getRandom(_sprites.length);
	}

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			return mass.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			auto meteoroid = cast(Meteoroid*) mass;
			_sprites[meteoroid._sprite_num].render(screen, meteoroid.pos);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Meteoroid meteoroid;
		foreach (int sprite_num; 0 .. _sprites.length)
		{
			meteoroid._sprite_num = sprite_num;
			meteoroid.render(screen, 0);
		}
	}

	enum kMaxNumInstances = 6;
	enum kZIndex = 2;
	enum kInitialV = Vector(0, 1);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 60;
	enum kDim = Vector(24, 24);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.Enemy;
	enum kCollisionDamage = 4;
	enum kExplosionSpeed = 8.0;
	enum kExplosionEnergy = 64;
	enum kScoreValue = 250;
	static immutable Sound[] explosion_sounds = [
		Sound.load!"meteoroid_explosion.raw",
	];

	private:

	static immutable _sprites = [
		PcxImage.load!"meteoroid/1.pcx",
		PcxImage.load!"meteoroid/2.pcx",
		PcxImage.load!"meteoroid/3.pcx",
	];

	int _sprite_num;
}

struct Truss
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Truss;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			return mass.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			_sprite.render(screen, mass.pos);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Truss truss;
		truss.render(screen, 0);
	}

	enum kMaxNumInstances = 32;
	enum kZIndex = 2;
	enum kInitialV = Vector(0, 1);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 100;
	enum kDim = Vector(17, 17);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.Enemy;
	enum kCollisionDamage = 10;
	enum kExplosionSpeed = 8.0;
	enum kExplosionEnergy = 64;
	enum kScoreValue = 1000;
	static immutable Sound[] explosion_sounds = [
		Sound.load!"truss_explosion.raw",
	];

	private:

	static immutable _sprite = PcxImage.load!"truss.pcx";
}
