module game.environment;

import os.geometry;
import os.graphics : Screen;
import os.system : Tick;
import os.utils : XorShiftRng;

import game.defs;
import game.player : Player;
import game.rigid_body : RigidBody;


struct Environment
{
	@nogc:
	nothrow:

	alias BodyVector = RigidBody*[kMaxNumBodies];
	@disable this();

	this(uint random_seed, ref ubyte[kStorageBufferSize][kMaxNumAllocations] dynamic_storage_buffers, BodyVector* body_vector, BodyVector* working_space_vector)
	{
		foreach (ref buffer; dynamic_storage_buffers[])
		{
			allocator.free(&buffer);
		}

		assert (body_vector != working_space_vector);
		_body_vector = body_vector;
		_working_space_vector = working_space_vector;
		_prng = XorShiftRng(random_seed);

		auto player_pos = Vector(kWorldWidth / 2, kWorldHeight - 1.5 * Player.kDim.y);
		_player = Player.make(&this, player_pos);
		assert (_player !is null);
	}

	@property Player* player() pure
	{
		return _player;
	}

	@property RigidBody*[] masses() pure
	{
		return (*_body_vector)[0 .. _num_masses];
	}

	@property Tick tick() const pure
	{
		return _tick;
	}

	@property uint score() const pure
	{
		return _score;
	}

	@property uint displayed_score() const pure
	{
		return _displayed_score;
	}

	void addScore(uint amount) pure
	{
		_score += amount;
	}

	int getRandom(int limit) pure
	{
		return _prng.getRandom(limit);
	}

	void addBody(RigidBody* mass) pure
	{
		assert (mass !is null);
		assert (_num_masses < kMaxNumBodies);
		(*_body_vector)[_num_masses] = mass;
		_num_masses++;
	}

	enum GameState
	{
		StillPlaying,
		Won,
		Lost,
	}

	@property GameState game_state() const pure
	{
		return _game_state;
	}

	void endGame(bool is_win) pure
	{
		player.state = RigidBody.State.GameOver;
		if (is_win)
		{
			_game_state = GameState.Won;
		}
		else
		{
			_game_state = GameState.Lost;
		}
	}

	void render(Screen screen) pure
	{
		// Bucket sort by Z index before rendering

		// We could manage this across cycles to avoid recalculating, but it's probably not worth the extra opportunities for
		// bugs.
		auto count_by_z_index = zIndexHistogram(masses[]);

		// Allocate buckets
		// Because we know exactly how many masses there are per Z index value, we can just slice up the temporary working
		// space
		RigidBody**[kMaxZIndex+1] z_index_bucket_ptrs;
		z_index_bucket_ptrs[0] = _working_space_vector.ptr;
		foreach (j; 1 .. kMaxZIndex+1)
		{
			z_index_bucket_ptrs[j] = z_index_bucket_ptrs[j-1] + count_by_z_index[j-1];
		}
		debug
		{
			RigidBody** list_end = z_index_bucket_ptrs[kMaxZIndex] + count_by_z_index[kMaxZIndex];
		}

		// Actual bucket sort
		foreach (mass; masses[])
		{
			*z_index_bucket_ptrs[mass.z_index]++ = mass;
		}
		debug
		{
			assert (z_index_bucket_ptrs[kMaxZIndex] == list_end);
			assert (list_end == _working_space_vector.ptr + _num_masses);
		}

		// Render
		foreach (mass; (*_working_space_vector)[0 .. _num_masses])
		{
			assert (mass !is null);
			mass.render(screen, _tick);
		}
	}

	void update(Tick tick)
	{
		_tick = tick;
		updateMasses();
		checkCollisions();

		if (_displayed_score < _score)
		{
			import std.algorithm : max;
			auto increment = max(1, (_score - _displayed_score) / 10);
			_displayed_score += increment;
		}
	}

	Allocator allocator;

	private:

	void updateMasses()
	{
		foreach (mass; masses[]) mass.update();

		foreach (ref mass; masses)
		{
			assert (mass !is null);
			auto keep = !mass.isGarbage() && mass.doAction(&this);
			if (!keep)
			{
				mass.destruct(&this);
				mass = null;
			}
		}

		/* Maintaining the array of masses is done as a batch job for efficiency.
		   Removing one element from an array, maintaining order, is O(N)
		   Removing k elements is O(k + N) if we null all k elements and then remove the nulls in a single sweep

		   Efficient collision detection is implemented by maintaining a sorted order.  We can get pseudo-linear time
		   by sorting on either axis, but sorting by Y seems to give a better worst case in this game.  The main point
		   is to keep time cost under control.

		   The array can be split into two parts: old masses that were sorted in the previous cycle, and new masses
		   that were added to the end this cycle in addBody().  We sort both parts separately and then merge.  Why?
		   To take advantage of existing sortedness.

		   Insertion sort is O(N^2) in the general case, but the old masses were sorted previously, then they all
		   moved by a small constant amount.  In this special case, insertion sort is O(N + num_overtakes) ~= O(N).
		   The new masses will happen to be somewhat sorted thanks the order they're added, but there'll only be a
		   few new masses per cycle, and insertion sort is about the most efficient O(N^2) sort anyway.

		   So, overall, we can maintain an array of sorted elements with insertions and removals in practically ~O(N)
		   time per cycle, which is all that's needed because rendering/updating the objects is already O(N) per cycle.
		   All the operations have good cache locality and are efficient on modern machines.
		*/

		auto old_masses = removeNulls(masses[0 .. _num_sorted_masses]).insertionSort!lessByY();
		auto new_masses = removeNulls(masses[_num_sorted_masses .. _num_masses]).insertionSort!lessByY();
		merge!lessByY(old_masses, new_masses, *_working_space_vector);
		_num_sorted_masses = _num_masses = old_masses.length + new_masses.length;

		import std.algorithm : swap;
		swap(_body_vector, _working_space_vector);
	}

	void checkCollisions()
	{
		import game.rigid_body : canCollide, collide, intersectX, intersectY;

		// I originally thought cheap rectangular collision detection would be enough for this demo, but it feels kind of
		// clunky now.  Oh well.

		debug
		{
			import std.algorithm : isSorted;
			foreach (mass; masses) assert (mass !is null);
			assert (isSorted!lessByY(masses));
		}

		// Taking advantage of the sorted order to get approximately O(N) performance.
		// It can blow out to O(N^2) if lots of objects have the same Y value, but that's not a problem yet.

		foreach (mass1_idx; 0 .. _num_sorted_masses - 1)
		{
			auto mass1 = masses[mass1_idx];
			assert (mass1 !is null);

			foreach (mass2_idx; mass1_idx + 1 .. _num_sorted_masses)
			{
				auto mass2 = masses[mass2_idx];
				assert (mass2 !is null);
				assert (mass1.pos.y <= mass2.pos.y);
				if (mass1.strictlyAbove(*mass2)) break;

				assert (intersectY(*mass1, *mass2));
				if (canCollide(*mass1, *mass2) && intersectX(*mass1, *mass2))
				{
					collide(&this, *mass1, *mass2);
					break;
				}
			}
		}
	}

	Player* _player;
	GameState _game_state = GameState.StillPlaying;
	uint _score = 0, _displayed_score = 0;
	Tick _tick;
	XorShiftRng _prng;
	size_t _num_masses = 0, _num_sorted_masses = 0;
	BodyVector* _body_vector, _working_space_vector;
}

bool lessByY(const(RigidBody)* a, const(RigidBody)* b) pure @nogc nothrow
{
	debug
	{
		import std.math : isNaN;
		assert (a !is null);
		assert (b !is null);
		assert (!isNaN(a.pos.y));
		assert (!isNaN(b.pos.y));
	}

	return a.pos.y < b.pos.y;
}

private:

version(unittest)
{
	bool intLess(int a, int b) @nogc nothrow
	{
		return a < b;
	}
}

T*[] removeNulls(T)(T*[] things) @nogc nothrow
{
	size_t num_things = 0;
	foreach (thing; things)
	{
		if (thing !is null)
		{
			things[num_things] = thing;
			++num_things;
		}
	}
	return things[0 .. num_things];
}

unittest
{
	int a, b;
	assert (removeNulls!int([]) == []);
	assert (removeNulls([&a]) == [&a]);
	assert (removeNulls!int([null]) == []);
	assert (removeNulls([&a, null]) == [&a]);
	assert (removeNulls([null, &a]) == [&a]);
	assert (removeNulls([&a, null, &b]) == [&a, &b]);
	assert (removeNulls([null, &a, null]) == [&a]);
	assert (removeNulls([null, &a, null, null, &b, null, null, null, null]) == [&a, &b]);

}

T[] insertionSort(alias less, T)(T[] things) @nogc nothrow
{
	size_t num_sorted = 0;
	foreach (thing; things[])
	{
		size_t j = num_sorted;
		while (j > 0 && less(thing, things[j - 1]))
		{
			things[j] = things[j-1];
			--j;
		}
		things[j] = thing;
		++num_sorted;
	}
	assert (num_sorted == things.length);
	return things;
}

unittest
{
	assert (insertionSort!intLess((int[]).init) == (int[]).init);
	assert (insertionSort!intLess([1]) == [1]);
	assert (insertionSort!intLess([1, 2, 3]) == [1, 2, 3]);
	assert (insertionSort!intLess([3, 2, 1]) == [1, 2, 3]);
	assert (insertionSort!intLess([5, 1, 1, 3, 3, 3, 2, 4]) == [1, 1, 2, 3, 3, 3, 4, 5]);
}

T[] merge(alias less, T)(T[] a, T[] b, T[] output) @nogc nothrow
{
	import std.array : empty, front, popFront;
	assert (output.length >= a.length + b.length);

	size_t total_length = a.length + b.length;
	auto output_ptr = output.ptr;

	while (!a.empty && !b.empty)
	{
		if (less(a.front, b.front))
		{
			*output_ptr++ = a.front;
			a.popFront();
		}
		else
		{
			*output_ptr++ = b.front;
			b.popFront();
		}
	}

	while (!b.empty)
	{
		*output_ptr++ = b.front;
		b.popFront();
	}

	while (!a.empty)
	{
		*output_ptr++ = a.front;
		a.popFront();
	}

	assert (output_ptr == output.ptr + total_length);

	return output[0 .. total_length];
}

unittest
{
	int[8] buffer;
	assert (merge!intLess([], [], buffer[]) == []);
	assert (merge!intLess([1], [], buffer[]) == [1]);
	assert (merge!intLess([], [1], buffer[]) == [1]);
	assert (merge!intLess([1, 2, 3], [1], buffer[]) == [1, 1, 2, 3]);
	assert (merge!intLess([1], [1, 2, 3], buffer[]) == [1, 1, 2, 3]);
	assert (merge!intLess([1, 3, 5], [2, 4], buffer[]) == [1, 2, 3, 4, 5]);
	assert (merge!intLess([1, 3, 5, 6, 7], [2, 4], buffer[]) == [1, 2, 3, 4, 5, 6, 7]);
}

int[kMaxZIndex+1] zIndexHistogram(RigidBody*[] masses) @nogc nothrow pure
{
	int[kMaxZIndex+1] histogram;
	foreach (mass; masses)
	{
		assert (mass.z_index <= kMaxZIndex);
		histogram[mass.z_index]++;
	}
	return histogram;
}
