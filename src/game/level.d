module game.level;
@nogc:
nothrow:

import os.system : Tick;
import os.geometry;

import game.enemies : Cannon, Meteoroid, Mine, Truss;
import game.environment : Environment;
import game.rigid_body;

/* The game level is randomly generated using a super-simple scripting system.  The level could be made consistent by setting a
   constant random seed, but randomness makes this simple game a little more interesting.

   The level has different areas, each with its own pattern of randomly generating enemies.  The areas are scheduled with the main
   script, which is a list of area-setting callbacks (LevelEvents) and times.  The callbacks initialise the area and set an area
   callback that's executed every game cycle to generate any enemies.

   Each LevelEvent includes a pointer to an immutable area specification structure that can be used to parameterise things like the
   proportions and rates of enemies to generate.

   The Level struct keeps track of the current area.

   It would have been nice to make this type safe using delegates, but statically allocating delegates to statically allocated
   data at compile time doesn't work well in the current version of D.  The classic C idiom of function-pointer-plus-void-pointer
   is used instead.
*/

struct Level
{
	@nogc:
	nothrow:

	this(immutable(LevelEvent[]) level_data) pure
	{
		_events = level_data;
	}

	void update(Environment* env, Tick time_since_start)
	{
		if (_event_idx < _events.length)
		{
			auto event = &_events[_event_idx];
			if (time_since_start == event.time)
			{
				event.callback(env, &this, event.args);
				++_event_idx;
			}
		}

		_area_update(env, _area_data);
	}

	alias AreaCallback = void function(Environment*, void*) @nogc nothrow pure;
	void setArea(AreaCallback area_update, void* area_data) pure
	{
		_area_update = area_update;
		_area_data = area_data;
	}

	private:

	size_t _event_idx = 0;
	immutable(LevelEvent[]) _events;

	AreaCallback _area_update = &doNothing;
	void* _area_data;
}

immutable LevelEvent[] level1_events = [
	LevelEvent(60, &singleMine, null),
	LevelEvent(120, &startMineField, &initial_mine_field1),
	LevelEvent(540, &startMineField, &initial_mine_field2),
	LevelEvent(1060, &startTrussRun, &truss_run_mine),
	LevelEvent(1280, &startTrussRun, &truss_run_mine_echelon),
	LevelEvent(1610, &startTrussRun, &truss_run_cannon),
	LevelEvent(3060, &startMineField, &mid_mine_field1),
	LevelEvent(4050, &startMineField, &mid_mine_field2),
	LevelEvent(6300, &startTrussRun, &truss_run_mine_echelon_final),
	LevelEvent(7100, &startTrussRun, &truss_run_cannon_final),
	LevelEvent(8200, &peaceAndQuiet, null),
	LevelEvent(8350, &win, null),
];

private:

void doNothing(Environment* env, void* data) pure {}

struct LevelEvent
{
	Tick time;
	void function(Environment*, Level* level, immutable(void*) args) @nogc nothrow callback;
	immutable(void*) args;
}

void peaceAndQuiet(Environment* env, Level* level, immutable(void*) unused)
{
	level.setArea(&doNothing, null);
}

void win(Environment* env, Level* level, immutable(void*) unused) pure
{
	env.endGame(true);
}

void singleMine(Environment* env, Level* level, immutable(void*) unused) pure
{
	makeMine(env);
	level.setArea(&doNothing, null);
}

// Mine field

struct MineFieldSpec
{
	int mine_odds, meteoroid_odds, cannon_odds;
	Tick interval_ticks;
}

struct MineFieldData
{
	Tick next_body_tick;
	immutable(MineFieldSpec)* spec;
}
__gshared MineFieldData mine_field_data;

void startMineField(Environment* env, Level* level, immutable(void*) spec)
{
	static void update(Environment* env, void* generic_data) @nogc nothrow pure
	{
		auto data = cast(MineFieldData*) generic_data;

		if (env.tick >= data.next_body_tick)
		{
			data.next_body_tick = env.tick + data.spec.interval_ticks;
			auto die_roll = env.getRandom(data.spec.mine_odds + data.spec.meteoroid_odds + data.spec.cannon_odds);
			if (die_roll < data.spec.mine_odds)
			{
				makeMine(env);
				data.next_body_tick += enemySpacing!Mine();
			}
			else if (die_roll < data.spec.mine_odds + data.spec.meteoroid_odds)
			{
				makeMeteoroid(env);
				data.next_body_tick += enemySpacing!Meteoroid();
			}
			else
			{
				makeCannon(env);
				data.next_body_tick += enemySpacing!Cannon();
			}
		}
	}

	mine_field_data.next_body_tick = env.tick;
	mine_field_data.spec = cast(immutable(MineFieldSpec)*) spec;
	level.setArea(&update, &mine_field_data);
}

immutable initial_mine_field1 = MineFieldSpec(2, 1, 0, 25);
immutable initial_mine_field2 = MineFieldSpec(2, 1, 0, 0);
immutable mid_mine_field1 = MineFieldSpec(2, 1, 1, 20);
immutable mid_mine_field2 = MineFieldSpec(2, 1, 2, 0);

// Truss run

struct TrussRunSpec
{
	Tick truss_interval_ticks;
	Tick enemy1_delay;
	void function(Environment*) @nogc nothrow pure make_enemy1;
	Tick enemy2_delay;
	void function(Environment*) @nogc nothrow pure make_enemy2;
}

struct TrussRunData
{
	Tick next_truss_tick, next_enemy1_tick, next_enemy2_tick;
	immutable(TrussRunSpec)* spec;
}
__gshared TrussRunData truss_run_data;

void startTrussRun(Environment* env, Level* level, immutable(void*) spec)
{
	static void update(Environment* env, void* generic_data) @nogc nothrow pure
	{
		auto data = cast(TrussRunData*) generic_data;

		if (env.tick >= data.next_truss_tick)
		{
			makeTrussLine(env);
			data.next_truss_tick += data.spec.truss_interval_ticks;
		}

		if (env.tick >= data.next_enemy1_tick)
		{
			data.spec.make_enemy1(env);
			data.next_enemy1_tick += data.spec.truss_interval_ticks;
		}

		if (data.spec.make_enemy2 !is null && env.tick >= data.next_enemy2_tick)
		{
			data.spec.make_enemy2(env);
			data.next_enemy2_tick += data.spec.truss_interval_ticks;
		}
	}

	auto truss_spec = cast(immutable(TrussRunSpec*)) spec;
	truss_run_data.spec = truss_spec;
	truss_run_data.next_truss_tick = env.tick;
	truss_run_data.next_enemy1_tick = env.tick + truss_spec.enemy1_delay;
	truss_run_data.next_enemy2_tick = env.tick + truss_spec.enemy2_delay;
	level.setArea(&update, &truss_run_data);
}

immutable truss_run_mine = TrussRunSpec(220, 70, &makeMine, 150, &makeLine!(Mine, 3));
immutable truss_run_mine_echelon = TrussRunSpec(110, 40, &makeMineEchelon);
immutable truss_run_cannon = TrussRunSpec(220, 80, &makeLine!(Cannon, 3), 140, &makeLine!(Meteoroid, 5));
immutable truss_run_mine_echelon_final = TrussRunSpec(110, 40, &makeMineEchelon);
immutable truss_run_cannon_final = TrussRunSpec(110, 40, &makeLine!(Cannon, 3));

enum kUnitsPerEchelon = 4;
enum kTrussesPerScreenWidth = cast(int)(kWorldWidth / Truss.kDim.x) + 2;

Tick enemySpacing(RB)() pure
{
	return cast(Tick) (RB.kDim.y / RB.kInitialV.y);
}

void makeMine(Environment* env) pure
{
	makeAtRandomX!Mine(env);
}

void makeMeteoroid(Environment* env) pure
{
	makeAtRandomX!Meteoroid(env);
}

void makeCannon(Environment* env) pure
{
	makeAtRandomX!Cannon(env);
}

void makeLine(RB, int kPitchFactor)(Environment* env) pure
{
	enum pitch = cast(int)(RB.kDim.x * kPitchFactor);
	auto x = cast(double) env.getRandom(pitch) - pitch / 2;
	auto pos = Vector(x, -RB.kDim.y + 1);

	foreach (_; 0 .. kWorldWidth/pitch + 1)
	{
		RB.make(env, pos);
		pos.x += pitch;
	}
}

void makeMineEchelon(Environment* env) pure
{
	switch (env.getRandom(2))
	{
		case 0:
			makeLeftEchelon!Mine(env);
			break;

		case 1:
			makeRightEchelon!Mine(env);
			break;

		default:
			assert (false);
	}
}

void makeLeftEchelon(RB)(Environment* env) pure
{
	double x = env.getRandom(cast(int)(kWorldWidth - 1 - RB.kDim.x * kUnitsPerEchelon));
	auto pos = Vector(x, -RB.kDim.y + 1.0);
	foreach (_; 0 .. kUnitsPerEchelon)
	{
		RB.make(env, pos);
		pos.x += RB.kDim.x;
		pos.y -= RB.kDim.y;
	}
}

void makeRightEchelon(RB)(Environment* env) pure
{
	double x = env.getRandom(cast(int)(kWorldWidth - 1 - RB.kDim.x * kUnitsPerEchelon)) + RB.kDim.x * kUnitsPerEchelon;
	auto pos = Vector(x, -RB.kDim.y + 1.0);
	foreach (_; 0 .. kUnitsPerEchelon)
	{
		RB.make(env, pos);
		pos -= RB.kDim;
	}
}

void makeTrussLine(Environment* env) pure
{
	import std.algorithm : max;
	int offset = env.getRandom(7);
	double x = -Truss.kDim.x / 2;
	foreach (truss_num; 0 .. kTrussesPerScreenWidth)
	{
		if ((truss_num + offset) % 7 < 3)
		{
			auto pos = Vector(x, -Truss.kDim.y);
			Truss.make(env, pos);
		}
		x += Truss.kDim.x;
	}
}
