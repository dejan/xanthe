module game.main;
@nogc:
nothrow:

import os.system;
import os.geometry;
import os.graphics;
import os.sound;
import os.utils;

import game.defs;
import game.effects;
import game.environment;
import game.level;
import game.player;
import game.rigid_body;

extern(C) void osMain()
{
	auto screen = getScreen();

	initSound(splash_music);

	palette.activate();

	splash_img.render(screen, 0, 0);
	flushScreenBuffer();
	fadeInSound(1.125);
	waitForKeyPress();

	bool play_again;
	do
	{
		fadeOutSound(0.9);
		delayTicks(15);
		playGame();
		while (true)
		{
			waitForKeyPress();
			if (isKeyDown(KeyCode.Space))
			{
				play_again = true;
				break;
			}

			if (isKeyDown(KeyCode.Escape))
			{
				play_again = false;
				break;
			}
		}
	} while (play_again);
	fadeOutSound(0.9);
	delayTicks(15);
}

void playGame()
{
	auto screen = getScreen();
	auto env = Environment(getRandomSeed(), dynamic_storage_buffers, &env_body_vector, &env_working_vector);
	auto level = Level(level1_events);

	setMusic(background_music);
	fadeInSound(1.1);
	initStarField(&env);

	auto cur_game_tick = getTick();
	const start_game_tick = cur_game_tick;
	bool game_ending = false;
	Tick game_end_tick;
	do
	{
		auto relative_tick = cur_game_tick - start_game_tick;
		// Misc. updates
		regenerateStars(&env);
		level.update(&env, relative_tick);

		if (env.game_state != Environment.GameState.StillPlaying && !game_ending)
		{
			game_ending = true;
			fadeOutSound(0.99);
			game_end_tick = cur_game_tick + 75;
		}

		// Physics
		env.update(relative_tick);

		updateSound();  // Extra check for sound buffering

		// Graphics
		background_img.render(screen, 0, 0);
		env.render(screen);

		renderScore(screen, env.displayed_score);
		env.player.renderHealth(screen);

		flushScreenBuffer();

		++cur_game_tick;
		waitForTick(cur_game_tick);
	} while (cur_game_tick != game_end_tick);

	fadeOutSound(0.9);
	delayTicks(10);
	final switch (env.game_state) with (Environment.GameState)
	{
		case StillPlaying:
			assert (false);

		case Won:
			setMusic(win_music);
			fadeInSound(1.007);
			game_clear_img.render(screen, 0, 0);
			flushScreenBuffer();
			break;

		case Lost:
			game_over_img.render(screen, 0, 0);
			flushScreenBuffer();
			break;
	}

	delayTicks(10);
}

auto littleEndianDigits(uint value) pure
{
	struct Digits
	{
		@nogc:
		nothrow:

		int opApply(scope int delegate(int) @nogc nothrow pure dg) pure
		{
			if (v == 0)
			{
				return dg(0);
			}

			int result = 0;
			while (v > 0)
			{
				result = dg(v % 10);
				if (result) break;
				v /= 10;
			}
			return result;
		}

		uint v;
	}
	return Digits(value);
}

unittest
{
	static bool passes(uint value, int[] golden) @nogc nothrow pure
	{
		size_t idx = 0;
		foreach (d; littleEndianDigits(value))
		{
			if (d != golden[idx++]) return false;
		}
		return idx == golden.length;
	}

	int[1] golden_0 = [0];
	assert (passes(0, golden_0[]));

	int[1] golden_9 = [9];
	assert (passes(9, golden_9[]));

	int[4] golden_1337 = [7, 3, 3, 1];
	assert (passes(1337, golden_1337[]));
}

void renderScore(Screen screen, uint score) pure
{
	enum kPitch = 10;
	enum kMargin = 3;

	uint x = kScreenWidth - kPitch - kMargin;

	foreach (d; littleEndianDigits(score))
	{
		assert (x >= 0);
		digit[d].render(screen, x, kMargin);
		x -= kPitch;
	}
}

unittest
{
	auto screen = getTestScreen();
	foreach (v; 0 .. 20)
	{
		renderScore(screen, v);
	}

	renderScore(screen, uint.max);
}

unittest
{
	auto screen = getTestScreen();
	background_img.render(screen, 0, 0);
	screen.checkIntegrity();
}

__gshared:
Environment.BodyVector env_body_vector, env_working_vector;

ubyte[kStorageBufferSize][kMaxNumAllocations] dynamic_storage_buffers;

immutable:
auto palette = Palette.load!"palette.col";
auto splash_img = PcxImage.load!"splash.pcx";
auto background_img = PcxImage.load!"background.pcx";
auto game_over_img = PcxImage.load!"game_over.pcx";
auto game_clear_img = PcxImage.load!"game_clear.pcx";
auto digit = [
	PcxImage.load!"font/0.pcx",
	PcxImage.load!"font/1.pcx",
	PcxImage.load!"font/2.pcx",
	PcxImage.load!"font/3.pcx",
	PcxImage.load!"font/4.pcx",
	PcxImage.load!"font/5.pcx",
	PcxImage.load!"font/6.pcx",
	PcxImage.load!"font/7.pcx",
	PcxImage.load!"font/8.pcx",
	PcxImage.load!"font/9.pcx",
];
auto splash_music = Sound.load!("moderato_intro.raw", true);
auto background_music = Sound.load!("moderato.raw", true);
auto win_music = Sound.load!("moderato_win.raw", true);
