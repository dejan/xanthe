module game.rigid_body;
@nogc:
nothrow:

public import os.geometry;
import os.graphics : Screen;
import os.sound : Sound;
import os.system : Tick;

import game.environment;
import game.player : Player;

struct RigidBody
{
	/* Because D classes don't work in this hard bare metal environment, RigidBody implements polymorphism using alias this and an
	   explicit vtable.  All physically modelled game entities (the player, enemies, missiles) derive from this struct using the
	   DeriveRigidBody mixin.  Unfortunately, "protected" doesn't work, so a lot of members end up being public.  This seems to just
	   be an oversight in the language, so maybe it will change in future versions of D.
    */
	   
	@nogc:
	nothrow:

	void update() pure
	{
		pos += v;
		v += a - v * _vtable.drag;
	}

	bool isActive() const pure
	{
		return state == State.Active;
	}

	bool isGarbage() const pure
	{
		return !_vtable.persist_after_explosion && !isActive();
	}

	bool doAction(Environment* env)
	{
		return _vtable.doAction(env, &this);
	}

	void render(Screen screen, Tick tick) pure
	{
		_vtable.render(screen, &this, tick);
	}

	@property Vector dim() const pure
	{
		return _vtable.dim;
	}

	@property size_t z_index() const pure
	{
		return _vtable.z_index;
	}

	@property string type_label() const pure
	{
		return _vtable.type_label;
	}

	@property uint type_id() const pure
	{
		return _vtable.type_id;
	}

	void takeDamage(Environment* env, int amount, uint hit_by_type)
	{
		if (isActive())
		{
			health -= amount;
			if (health <= 0)
			{
				import game.effects : sparkShower;
				import game.player : Missile, Player;
				if (hit_by_type == Missile.kTypeId)
				{
					env.addScore(_vtable.score_value);
				}
				if (type_id == Player.kTypeId)
				{
					env.endGame(false);
				}
				state = State.Exploding;
				sparkShower(env, pos + dim/2, _vtable.explosion_energy, _vtable.explosion_speed);
				if (_vtable.explosion_sounds.length)
				{
					auto idx = env.getRandom(cast(int)_vtable.explosion_sounds.length);
					(*_vtable.explosion_sounds)[idx].play();
				}
			}
		}
	}

	void destruct(Environment* env)
	{
		env.allocator.free(cast(void*)&this);
	}

	bool strictlyToLeft(const ref RigidBody o) const pure
	{
		return pos.x + dim.x <= o.pos.x;
	}

	bool strictlyToRight(const ref RigidBody o) const pure
	{
		return pos.x >= o.pos.x + o.dim.x;
	}

	bool strictlyAbove(const ref RigidBody o) const pure
	{
		return pos.y + dim.y <= o.pos.y;
	}

	bool strictlyBelow(const ref RigidBody o) const pure
	{
		return pos.y >= o.pos.y + o.dim.y;
	}

	enum State
	{
		Active,
		Exploding,
		GameOver,
	}

	enum Team
	{
		None = 0,
		Player = 0b01,
		Enemy = 0b10,
	}

	struct VTable
	{
		@nogc:
		nothrow:

		bool function(Environment* env, RigidBody* self) @nogc nothrow doAction;
		void function(Screen screen, RigidBody* self, Tick tick) @nogc nothrow pure render;

		Vector dim;
		size_t z_index;
		double drag = 0.0;
		int max_health;
		Team team;
		int collision_damage;
		double explosion_speed = 0.0;
		int explosion_energy;
		bool persist_after_explosion;
		uint score_value = 0;
		string type_label;
		uint type_id;
		immutable(Sound[]*) explosion_sounds;
	}

	Vector pos, v, a;
	State state = State.Active;
	int health;

	private:
	immutable(VTable*) _vtable;
}

mixin template DeriveRigidBody(Derived)
{
	alias mass this;

	public
	{
		import os.utils : djb2XorHash;
		enum kTypeId = djb2XorHash(__traits(identifier, Derived));

		static Derived* make(Environment* env, const ref Vector pos) pure
		{
			auto instance = env.allocator.emplace!Derived(pos);
			if (instance !is null)
			{
				static if (__traits(hasMember, Derived, "setup"))
				{
					instance.setup(env);
				}
				env.addBody(cast(RigidBody*)instance);
			}
			return instance;
		}

		this(Vector pos_) pure
		{
			mass.pos = pos_;
		}

		RigidBody mass = { v: kInitialV, a: kInitialA, health: kInitialHealth, _vtable: &vtable };
	}

	private
	{
		static immutable RigidBody.VTable vtable = {
			dim: kDim,
			z_index: kZIndex,
			drag: kDrag,
			team: kTeam,
			collision_damage: getMember!(Derived, "kCollisionDamage")(0),
			explosion_speed: getMember!(Derived, "kExplosionSpeed")(0),
			explosion_energy: getMember!(Derived, "kExplosionEnergy")(0),
			persist_after_explosion: getMember!(Derived, "kPersistAfterExplosion")(false),
			score_value: getMember!(Derived, "kScoreValue")(0),
			doAction: &VFuncs.doAction,
			render: &VFuncs.render,
			type_label: __traits(identifier, Derived),
			type_id: kTypeId,
			explosion_sounds: &Derived.explosion_sounds,
		};

		static __gshared int _num_instances = 0;
	}
}

RB* makeAtRandomX(RB)(Environment* env, double y = -RB.kDim.y + 1.0)
{
	auto x = env.getRandom(cast(int)(kWorldWidth - 1)) - RB.kDim.x / 2;
	auto pos = Vector(x, y);
	return RB.make(env, pos);
}

bool canCollide(ref RigidBody a, ref RigidBody b) pure
{
	return (a._vtable.team | b._vtable.team) == (RigidBody.Team.Player | RigidBody.Team.Enemy);
}

unittest
{
	import game.effects : Spark;
	import game.enemies : Mine, Orb;
	import game.player : Player, Missile;

	Vector pos;

	auto spark = Spark(pos);
	auto mine = Mine(pos);
	auto orb = Orb(pos);
	Player player;
	auto missile = Missile(pos);

	assert (!canCollide(spark, mine));
	assert (!canCollide(player, spark));
	assert (!canCollide(spark, orb));

	assert (!canCollide(mine, mine));
	assert (!canCollide(mine, orb));

	assert (canCollide(mine, missile));

	assert (!canCollide(missile, missile));
	assert (!canCollide(missile, player));

	assert (canCollide(player, orb));
}

void collide(Environment* env, ref RigidBody a, ref RigidBody b)
{
	assert (canCollide(a, b));
	a.takeDamage(env, b._vtable.collision_damage, b.type_id);
	b.takeDamage(env, a._vtable.collision_damage, a.type_id);
}

bool intersectX(const ref RigidBody a, const ref RigidBody b) pure
{
	return !a.strictlyToLeft(b) && !a.strictlyToRight(b);
}

bool intersectY(const ref RigidBody a, const ref RigidBody b) pure
{
	return !a.strictlyAbove(b) && !a.strictlyBelow(b);
}

unittest
{
	import game.player : Player;
	auto width = Player.kDim.x;
	auto a = Player(Vector(0, 0)), b = Player(Vector(width/2, 0)), c = Player(Vector(width, 0));

	assert (!a.strictlyToLeft(b));
	assert (!a.strictlyToRight(b));
	assert (!b.strictlyToLeft(a));
	assert (!b.strictlyToRight(a));

	assert (a.strictlyToLeft(c));
	assert (!a.strictlyToRight(c));
	assert (c.strictlyToRight(a));
	assert (!c.strictlyToLeft(a));

	assert (intersectX(a, b));
	assert (intersectX(b, a));
	assert (!intersectX(a, c));
	assert (!intersectX(c, a));
}

unittest
{
	import game.player : Player;
	auto height = Player.kDim.y;
	auto a = Player(Vector(0, 0)), b = Player(Vector(0, height/2)), c = Player(Vector(0, height));

	assert (!a.strictlyAbove(b));
	assert (!a.strictlyBelow(b));
	assert (!b.strictlyAbove(a));
	assert (!b.strictlyBelow(a));

	assert (a.strictlyAbove(c));
	assert (!a.strictlyBelow(c));
	assert (c.strictlyBelow(a));
	assert (!c.strictlyAbove(a));

	assert (intersectY(a, b));
	assert (intersectY(b, a));
	assert (!intersectY(a, c));
	assert (!intersectY(c, a));
}


auto ref getMember(Struct, alias member_name, T)(auto ref T default_value) pure
{
	static if (__traits(hasMember, Struct, member_name))
	{
		mixin("return Struct." ~ member_name ~ ";");
	}
	else
	{
		return default_value;
	}
}
