module game.effects;

@nogc:
nothrow:

import std.meta : AliasSeq;

import os.graphics;
import os.sound : Sound;
import os.system : Tick;

import game.environment;
import game.rigid_body;

void sparkShower(Environment* env, Vector origin, int count, double max_speed)
{
	assert (count > 0);

	static double getSpeed(Environment* env, double max_speed) @nogc nothrow
	{
		return max_speed * cast(double)(env.getRandom(1025) - 512) / 512;
	}

	static int jitter(Environment* env) @nogc nothrow
	{
		return env.getRandom(3) - 1;
	}

	while (count--)
	{
		auto spark_pos = origin + Vector(jitter(env), jitter(env));
		auto spark = Spark.make(env, spark_pos);
		if (spark is null) break;
		auto vx = getSpeed(env, max_speed);
		auto vy = getSpeed(env, max_speed);
		spark.v = Vector(vx, vy);
	}
}

struct Spark
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Spark;

	void setup(Environment* env) pure
	{
		color_base = cast(ubyte)(kFirstGradient + kGradientSize/2 + env.getRandom(kNumGradients) * kGradientSize);
		color_phase = cast(ubyte) env.getRandom(kGradientSize / 2);
	}

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			if (mass.pos.x < 0 || mass.pos.x >= kWorldWidth || mass.pos.y < 0 || mass.pos.y >= kWorldHeight)
			{
				return false;
			}
			return true;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			auto spark = cast(Spark*) mass;
			if (spark.pos.x >= 0 && spark.pos.x < kWorldWidth && spark.pos.y >= 0 && spark.pos.y < kWorldHeight)
			{
				ubyte c = cast(ubyte)(((tick + spark.color_phase) % (kGradientSize / 2)) + spark.color_base);
				putPixel(screen, spark.pos, c);
			}
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Spark spark;
		spark.render(screen, 0);
	}

	enum kMaxNumInstances = 256;
	enum kZIndex = 5;
	enum kInitialV = Vector(0, 0);
	enum kInitialA = Vector(0, -0.1);
	enum kInitialHealth = 1;
	enum kDim = Vector(1, 1);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.None;
	static immutable Sound[] explosion_sounds = [];

	private:

	enum kGradientSize = 32;
	enum kFirstGradient = 112;
	enum kNumGradients = 3;

	ubyte color_base = kFirstGradient + kGradientSize/2;
	ubyte color_phase;
}

struct NearStar
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!NearStar;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			return mass.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			auto star = cast(NearStar*) mass;
			putPixel(screen, star.pos, 47);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		NearStar star;
		star.render(screen, 0);
	}

	enum kMaxNumInstances = 16;
	enum kZIndex = 1;
	enum kInitialV = Vector(0, .01);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 1;
	enum kDim = Vector(1, 1);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.None;
	static immutable Sound[] explosion_sounds = [];
}

struct FarStar
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!FarStar;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			return mass.pos.y < kWorldHeight;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			auto star = cast(FarStar*) mass;
			putPixel(screen, star.pos, 36);
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		FarStar star;
		star.render(screen, 0);
	}

	enum kMaxNumInstances = 64;
	enum kZIndex = 0;
	enum kInitialV = NearStar.kInitialV / 2;
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 1;
	enum kDim = Vector(1, 1);
	enum kDrag = 0.0;
	enum kTeam = RigidBody.Team.None;
	static immutable Sound[] explosion_sounds = [];
}

alias StarTypes = AliasSeq!(FarStar, NearStar);

void initStarField(Environment* env)
{
	foreach (StarType; StarTypes)
	{
		foreach (_; 0 .. StarType.kMaxNumInstances - 4)
		{
			double y = env.getRandom(cast(int)kWorldHeight);
			makeAtRandomX!StarType(env, y);
		}
	}
}

void regenerateStars(Environment* env)
{
	foreach (StarType; StarTypes)
	{
		if (env.getRandom(cast(int)(kWorldHeight / StarType.kMaxNumInstances / StarType.kInitialV.y)) == 0)
		{
			makeAtRandomX!StarType(env);
		}
	}
}
