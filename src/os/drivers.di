// D import file generated from 'src/os/drivers-bios.d'
module os.drivers;
@nogc nothrow 
{
	import os.system : ColorSpec, incrementTick, KeyCode, kScreenHeight, kScreenWidth, setKeyState;
	extern (C) void osMain();
	void waitForEvent();
	void flushScreenBuffer();
	void setPalette(ref const ColorSpec[256] palette);
	package 
	{
		ubyte* getScreenBuffer();
		void initSoundBackend(uint samples_per_second, ushort num_buffer_samples);
		private 
		{
			enum kCom1DataPort = 1016;
			enum kCom1LineStatusPort = 1021;
			enum kPic1CommandPort = 32;
			enum kPic1DataPort = 33;
			enum kPic2CommandPort = 160;
			enum kPic2DataPort = 161;
			enum kPit0DataPort = 64;
			enum kKeyboardDataPort = 96;
			enum kVgaDacAddrPort = 968;
			enum kVgaDacDataPort = 969;
			enum kSb16ResetPort = 550;
			enum kSb16ReadPort = 554;
			enum kSb16WritePort = 556;
			enum kSb16ReadBufferStatusPort = 558;
			enum kSb16MixerAddrPort = 548;
			enum kSb16MixerDataPort = 549;
			enum kSb16InterruptStatusPort = 559;
			enum kDma2MaskPort = 212;
			enum kDma2ModePort = 214;
			enum kDma2ClearFlipFlopPort = 216;
			enum kDma2Ch5AddrPort = 196;
			enum kDma2Ch5CountPort = 198;
			enum kDma2Ch5PagePort = 139;
			enum kSegCode32 = 8;
			enum kSegData = 16;
			enum ksegCode16 = 24;
			enum screen_ptr = cast(void*)655360;
			__gshared ubyte[kScreenWidth * kScreenHeight] screen_buffer;
			extern (C) void systemEntry();
			void outPortByte(ushort port, ubyte value);
			ubyte inPortByte(ushort port);
			void disableInterrupts();
			void enableInterrupts();
			void timerIsr();
			void keyboardIsr();
			void handleKeyboardIrq();
			void soundIsr();
			extern (C) extern __gshared short sb16_buffer;
			__gshared short* sb16_next_load;
			__gshared short* sb16_now_playing;
			__gshared ushort sb16_num_buffer_samples;
			void handleSb16Irq();
			struct IdtEntry
			{
				align (1)@nogc nothrow 
				{
					ushort offset_lo;
					ushort segment;
					ubyte zero = 0;
					ubyte flags;
					ushort offset_hi;
					static pure IdtEntry interrupt(void function() isr);
				}
			}
			__gshared IdtEntry[48] idt;
			struct IdtPointer
			{
				align (1)
				{
					ushort limit;
					IdtEntry* idt;
				}
			}
			auto __gshared idtr = IdtPointer(idt.sizeof - 1, idt.ptr);
		}
	}
}
