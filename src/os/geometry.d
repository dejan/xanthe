module os.geometry;

import std.traits : isNumeric;

enum kWorldWidth = 320.0;
enum kWorldHeight = 200.0;

struct Vector
{
	double x, y;

	Vector opBinary(string op)(Vector other) const
	{
		mixin("return Vector(x" ~ op ~ "other.x, y" ~ op ~ "other.y);");
	}

	Vector opBinary(string op, S)(S scalar) const if (isNumeric!S)
	{
		mixin("return Vector(x" ~ op ~ "scalar, y" ~ op ~ "scalar);");
	}

	void opOpAssign(string op)(Vector other)
	{
		mixin("x " ~ op ~ "= other.x;");
		mixin("y " ~ op ~ "= other.y;");
	}

	void opOpAssign(string op, S)(S scalar) if (isNumeric!S)
	{
		mixin("x " ~ op ~ "= scalar;");
		mixin("y " ~ op ~ "= scalar;");
	}
}
