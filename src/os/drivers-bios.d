module os.drivers;
@nogc:
nothrow:

// The BIOS bootloader starts D code execution from systemEntry().

import os.system : ColorSpec, incrementTick, KeyCode, kScreenHeight, kScreenWidth, setKeyState;

extern(C) void osMain();

void waitForEvent()
{
	asm @nogc nothrow
	{
		hlt;
	}
}

void flushScreenBuffer()
{
	uint* src = cast(uint*) screen_buffer.ptr, dst = cast(uint*) screen_ptr;
	uint num_uints = kScreenWidth * kScreenHeight / uint.sizeof;
	while (num_uints--)
	{
		*dst++ = *src++;
	}
}

void setPalette(const ref ColorSpec[256] palette)
{
	auto data = cast(immutable(ubyte)*) palette.ptr;
	foreach (ubyte j; 0..256)
	{
		// Pallette index
		outPortByte(kVgaDacAddrPort, j);
		// R, G and B values on a scale of 0-63
		outPortByte(kVgaDacDataPort, *data++ >> 2);
		outPortByte(kVgaDacDataPort, *data++ >> 2);
		outPortByte(kVgaDacDataPort, *data++ >> 2);
	}
}

package:

ubyte* getScreenBuffer()
{
	return screen_buffer.ptr;
}

void initSoundBackend(uint samples_per_second, ushort num_buffer_samples)
{
	// The definitive manual for the Sound Blaster 16 is the "Sound Blaster Series Hardware Programming Guide"
	// https://pdos.csail.mit.edu/6.828/2014/readings/hardware/SoundBlaster.pdf

	/* This driver uses the popular two-buffer, auto-initialised mode to make glitch-free audio.  sb16_buffer is twice the nominal
	   audio buffer size, and the DMA is programmed to pump it continually into the sound card in a loop (auto-initialised mode).
	   If all works well, at any given moment, one half of sb16_buffer is being played by the sound card, and the other half can be 
	   written to as the current audio buffer.

	   To achieve this, the SB16 is programmed to generate an interrupt at twice the rate the DMA loops through sb16_buffer.  The
	   handler for this interrupt triggers loading of more audio data, and swaps the sb16_now_playing and sb16_next_load pointers.
    */

	sb16_num_buffer_samples = num_buffer_samples;

	// Initialise and detect Sound Blaster card
	{
		import os.system : getTick, waitForTick;
		auto until = getTick() + 1;
		outPortByte(kSb16ResetPort, 1);
		waitForTick(until);
		until++;
		outPortByte(kSb16ResetPort, 0);

		// The SB16 will send us an 0xaa when it's ready.  However, we need to wait for the data read ready flag before reading anything,
		// and there's no guarantee there's even an SB16 around to set the flag.
		auto max_read_cycles = 5;
		ubyte sb_read_status;
		do
		{
			do
			{
				waitForTick(until);
				until++;
				sb_read_status = inPortByte(kSb16ReadBufferStatusPort);
				if (max_read_cycles-- == 0) return;  // SB16 still not found.  Give up.
			} while (!(sb_read_status & 0b10000000));
		} while (inPortByte(kSb16ReadPort) != 0xaa);
	}

	static void sb16Write(ubyte data) @nogc nothrow
	{
		ubyte sb_data;
		do
		{
			sb_data = inPortByte(kSb16WritePort);
		} while (sb_data);
		outPortByte(kSb16WritePort, data);
	}

	// Fill audio buffer with silence
	{
		// There are actually two buffers side by side.  We clear them both quickly by writing 4B uints instead of 2B ushorts.
		auto buffer = cast(uint*)&sb16_buffer;
		foreach (_; 0 .. num_buffer_samples)
		{
			*buffer++ = 0;
		}
	}

	disableInterrupts();

	sb16_next_load = &sb16_buffer;
	sb16_now_playing = &sb16_buffer + num_buffer_samples;

	// Configure DMA
	outPortByte(kDma2MaskPort, 5);
	outPortByte(kDma2ClearFlipFlopPort, 0);
	outPortByte(kDma2ModePort, 0x58 + 5);  // Auto-initialised playback
	outPortByte(kDma2Ch5AddrPort, 0);
	outPortByte(kDma2Ch5AddrPort, 0);
	outPortByte(kDma2Ch5CountPort, cast(ubyte)((2 * num_buffer_samples - 1) & 0xff));
	outPortByte(kDma2Ch5CountPort, cast(ubyte)((2 * num_buffer_samples - 1) >> 8));

	auto sb16_buffer_page = cast(ubyte)((cast(uint)&sb16_buffer) >> 16);
	outPortByte(kDma2Ch5PagePort, sb16_buffer_page);
	outPortByte(kDma2MaskPort, 1);

	// Configure interrupts using IRQ 5 (qemu doesn't seem to support SB on other IRQs)
	outPortByte(kSb16MixerAddrPort, 0x80);
	outPortByte(kSb16MixerDataPort, 0x02);
	idt[0x25] = IdtEntry.interrupt(&soundIsr);
	asm @nogc nothrow
	{
		// unmask IRQ 5
		in AL, kPic1DataPort;
		and AL, 0b11011111;
		out kPic1DataPort, AL;
	}

	// Configure playback
	sb16Write(0x41);
	sb16Write(cast(ubyte)(samples_per_second >> 8));
	sb16Write(cast(ubyte)(samples_per_second & 0xff));

	sb16Write(0xb6); // Auto-initialised 16b output
	sb16Write(0x10); // Mono signed
	sb16Write(cast(ubyte)((num_buffer_samples - 1) & 0xff));
	sb16Write(cast(ubyte)((num_buffer_samples - 1) >> 8));
	enableInterrupts();
}

private:

// Serial Port
enum kCom1DataPort = 0x3f8;
enum kCom1LineStatusPort = 0x3fd;

// Programmable Interrupt Controller
enum kPic1CommandPort = 0x20;
enum kPic1DataPort = 0x21;
enum kPic2CommandPort = 0xa0;
enum kPic2DataPort = 0xa1;

// Programmable Interval Timer
enum kPit0DataPort = 0x40;

enum kKeyboardDataPort = 0x60;

enum kVgaDacAddrPort = 0x3c8;
enum kVgaDacDataPort = 0x3c9;

// Sound Blaster 16
enum kSb16ResetPort = 0x226;
enum kSb16ReadPort = 0x22a;
enum kSb16WritePort = 0x22c;
enum kSb16ReadBufferStatusPort = 0x22e;
enum kSb16MixerAddrPort = 0x224;
enum kSb16MixerDataPort = 0x225;
enum kSb16InterruptStatusPort = 0x22f;

// DMA
enum kDma2MaskPort = 0xd4;
enum kDma2ModePort = 0xd6;
enum kDma2ClearFlipFlopPort = 0xd8;
enum kDma2Ch5AddrPort = 0xc4;
enum kDma2Ch5CountPort = 0xc6;
enum kDma2Ch5PagePort = 0x8b;

// Protected mode memory segments (defined in GDT)
enum kSegCode32 = 0x08;
enum kSegData = 0x10;
enum ksegCode16 = 0x18;

enum screen_ptr = cast(void*) 0xa0000;
__gshared ubyte[kScreenWidth * kScreenHeight] screen_buffer;

extern(C) void systemEntry()
{
	idt[0x20] = IdtEntry.interrupt(&timerIsr);
	idt[0x21] = IdtEntry.interrupt(&keyboardIsr);

	asm @nogc nothrow
	{
		lidt idtr;

		// Program the timer to 50Hz (default 18.2)
		// Every system "tick" is now 20ms
		mov AL, 0x3c;
		out kPit0DataPort, AL;
		mov AL, 0x5d;
		out kPit0DataPort, AL;

		mov AL, 0b11111100; // Unmask keyboard and timer IRQs
		out kPic1DataPort, AL;
	}
	enableInterrupts();
	osMain();
}

void outPortByte(ushort port, ubyte value)
{
	asm @nogc nothrow
	{
		push EAX;
		push EDX;
		mov DX, port;
		mov AL, value;
		out DX, AL;
		pop EDX;
		pop EAX;
	}
}

ubyte inPortByte(ushort port)
{
	asm @nogc nothrow
	{
		push EDX;
		mov DX, port;
		in AL, DX;
		pop EDX;
	}
}

void disableInterrupts()
{
	asm @nogc nothrow
	{
		cli;
	}
}

void enableInterrupts()
{
	asm @nogc nothrow
	{
		sti;
	}
}

void timerIsr()
{
	asm @nogc nothrow
	{
		naked;
		push EAX;
		push ECX;
		push EDX;
		call incrementTick;

		// Clear IRQ
		mov AL, 0x20;
		out kPic1CommandPort, AL;
		pop EDX;
		pop ECX;
		pop EAX;
		iretd;
	}
}

void keyboardIsr()
{
	asm @nogc nothrow
	{
		naked;
		push EAX;
		push ECX;
		push EDX;
		call handleKeyboardIrq;

		// Clear IRQ
		mov AL, 0x20;
		out kPic1CommandPort, AL;
		pop EDX;
		pop ECX;
		pop EAX;
		iretd;
	}
}

void handleKeyboardIrq()
{
	auto key_data = inPortByte(kKeyboardDataPort);
	ubyte scan_code = key_data & 0x7f;
	auto is_down = !(key_data & 0x80);
	switch (scan_code)
	{
		case 0x39:
			setKeyState(KeyCode.Space, is_down);
			break;
		case 0x4b:
			setKeyState(KeyCode.Left, is_down);
			break;
		case 0x4d:
			setKeyState(KeyCode.Right, is_down);
			break;
		case 0x01:
			setKeyState(KeyCode.Escape, is_down);
			break;
		default:
			setKeyState(KeyCode.Other, is_down);
			break;
	}
}

void soundIsr()
{
	asm @nogc nothrow
	{
		naked;
		push EAX;
		push ECX;
		push EDX;

		// According to the SB16 manual, we need to check if this really is an SB16 interrupt, and it ignore it othewise.
		mov DX, kSb16MixerAddrPort;
		mov AL, 0x82; // Interrupt status register
		out DX, AL;
		mov DX, kSb16MixerDataPort;
		in AL, DX;
		test AL, 2;
		jz isr_return;

		call handleSb16Irq;

		// Acknowledge SB16 Interupt
		mov DX, kSb16InterruptStatusPort;
		in AL, DX;

		isr_return:

		// Clear IRQ
		mov AL, 0x20;
		out kPic1CommandPort, AL;
		pop EDX;
		pop ECX;
		pop EAX;
		iretd;
	}
}

extern(C)
{
	extern __gshared short sb16_buffer;
}
__gshared short* sb16_next_load;
__gshared short* sb16_now_playing;
__gshared ushort sb16_num_buffer_samples;

void handleSb16Irq()
{
	import os.sound : outputBufferEmptyCallback;
	import std.algorithm : swap;

	outputBufferEmptyCallback(sb16_next_load, sb16_num_buffer_samples);
	swap(sb16_next_load, sb16_now_playing);
}

struct IdtEntry
{
	align (1):
	@nogc:
	nothrow:
	ushort offset_lo;
	ushort segment;
	ubyte zero = 0;
	ubyte flags;
	ushort offset_hi;

	static IdtEntry interrupt(void function() isr) pure
	{
		/* Unfortunately, the IDT data structure splits the offset pointer into two, which means we can't easily initialise the
		   interrupt vector table at compile time because the offset pointer values aren't known until link time.

		   If the IVT really needed to be stored in a ROM, or something, we could do some extra linker scripting, but then we'd
		   probably also be using a nicer platform than x86.  I decided to just suck it up and initialise the IVT at run time.
	    */
		static assert (typeof(isr).sizeof == 4);
		auto offset = cast(uint) isr;
		auto offset_lo = cast(ushort)(offset & 0xffff);
		auto offset_hi = cast(ushort)(offset >> 16);
		return IdtEntry(offset_lo, kSegCode32, 0, 0b10001110, offset_hi);
	}
}
__gshared IdtEntry[0x30] idt;

struct IdtPointer
{
	align(1):
	ushort limit;
	IdtEntry* idt;
}
__gshared auto idtr = IdtPointer(idt.sizeof - 1, idt.ptr);
