module os.utils;

struct FreeList(alias kMaxAllocSize)
{
	// A simple freelist-based memory allocator
	@nogc:
	nothrow:

	static assert(kMaxAllocSize > (void*).sizeof);

	void free(T : void)(T *memory) pure
	{
		debug
		{
			import core.stdc.string : memset;
			memset(memory, 0xab, kMaxAllocSize);
		}
		*(cast(void**) memory) = _next_free;
		_next_free = memory;
	}

	void free(T)(T *memory) pure
	{
		static assert(T.sizeof >= kMaxAllocSize);
		free(cast(void*) memory);
	}

	T* emplace(T, Args...)(auto ref Args args)
	{
		import conv = std.conv;
		static assert(T.sizeof <= kMaxAllocSize);
		auto ptr = cast(T*) alloc();
		if (ptr !is null)
		{
			conv.emplace(ptr, args);
		}
		return ptr;
	}

	void* alloc() pure
	{
		void* allocated = _next_free;
		if (allocated !is null)
		{
			_next_free = *(cast(void**) allocated);
		}
		return allocated;
	}

	private:
	void* _next_free = null;
}

struct XorShiftRng
{
	@nogc:
	nothrow:

	this(uint seed) pure
	{
		if (seed == 0) --seed;
		_state = seed;
	}

	int getRandom(int limit) pure
	{
		nextRandom();
		return cast(int) (cast(double)(_state - 1) * limit / uint.max);
	}

	private:

	void nextRandom() pure
	{
		_state ^= _state << 13;
		_state ^= _state >> 17;
		_state ^= _state << 5;
	}

	uint _state = 1;
}

uint djb2XorHash(string s) pure @nogc nothrow
{
	// http://www.cse.yorku.ca/~oz/hash.html
	uint hash = 5381;
	foreach (char c; s)
	{
		hash *= 33;
		hash ^= c;
	}
	return hash;
}
