module os.drivers;

import std.exception : enforce;

import sdl2;

import os.system : ColorSpec, incrementTick, KeyCode, kScreenHeight, kScreenWidth, setKeyState;

extern(C) void osMain();

void waitForEvent() @nogc nothrow
{
	SDL_Event event = void;
	auto okay = SDL_WaitEvent(&event);
	if (!okay)
	{
		showSdlError();
		exit(2);
	}

	switch (event.type)
	{
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			auto is_down = event.key.state == SDL_PRESSED;
			switch (event.key.keysym.scancode)
			{
				case SDL_SCANCODE_SPACE:
					setKeyState(KeyCode.Space, is_down);
					break;
				case SDL_SCANCODE_LEFT:
					setKeyState(KeyCode.Left, is_down);
					break;
				case SDL_SCANCODE_RIGHT:
					setKeyState(KeyCode.Right, is_down);
					break;
				case SDL_SCANCODE_ESCAPE:
					setKeyState(KeyCode.Escape, is_down);
					break;
				default:
					setKeyState(KeyCode.Other, is_down);
					break;
			}
			break;

		case SDL_USEREVENT:
			incrementTick();
			break;

		case SDL_QUIT:
			exit(0);
			break;

		default:
			// Ignore other events;
	}
}

void flushScreenBuffer() @nogc nothrow
{
	// SDL2 doesn't support writing indexed 8b colour to the screen any more.
	// This seems to be as direct as is possible.  Oh well.

	SDL_BlitSurface(surface8, null, surface32, null);

	void* pixel_data;
	int pixel_data_pitch;
	SDL_LockTexture(texture, null, &pixel_data, &pixel_data_pitch);
	SDL_ConvertPixels(kScreenWidth, kScreenHeight, surface32.format.format, surface32.pixels, surface32.pitch, kTextureFormat, pixel_data, pixel_data_pitch);
	SDL_UnlockTexture(texture);

	SDL_RenderCopy(renderer, texture, null, null);
	SDL_RenderPresent(renderer);
}

void setPalette(const ref ColorSpec[256] palette) @nogc nothrow
{
	SDL_Color[256] sdl_palette;
	foreach (j; 0..256)
	{
		auto c = palette[j];
		sdl_palette[j] = SDL_Color(c.r, c.g, c.b, 0xff);
	}
	SDL_SetPaletteColors(surface8.format.palette, sdl_palette.ptr, 0, 256);
}

package:

ubyte* getScreenBuffer() @nogc nothrow
{
	return cast(ubyte*) surface8.pixels;
}

void initSoundBackend(uint samples_per_second, ushort num_buffer_samples) @nogc nothrow
{
	extern(C) static callback(void* userdata, ubyte* stream, int len) @nogc nothrow
	{
		import os.sound : outputBufferEmptyCallback;
		assert ((len & 1) == 0);
		outputBufferEmptyCallback(cast(short*)stream, len/2);
	}

	SDL_AudioSpec format;
	format.freq = samples_per_second;
	format.format = AUDIO_S16LSB; 
	format.channels = 1;
	format.samples = num_buffer_samples;
	format.userdata = null;
	format.callback = &callback;

	auto device = SDL_OpenAudioDevice(null, 0, &format, &format, 0);
	if (!device)
	{
		showSdlError();
		exit(3);
	}
	SDL_PauseAudioDevice(device, 0);
}

private:

enum kTextureFormat = SDL_PIXELFORMAT_RGBA8888;

import io = std.stdio;
import core.stdc.stdlib : atexit, exit;

__gshared SDL_Window* window;
__gshared SDL_Renderer* renderer;
__gshared SDL_Texture* texture;
__gshared SDL_Surface* surface8, surface32;

struct Config
{
	bool test = false;
	bool fullscreen = true;
}

int main(string[] args)
{
	import std.array;
	Config config;
	// The current getopt implementation in Phobos is a bit problematic.
	args.popFront;  // Skip executable name
	while (!args.empty)
	{
		import std.algorithm : startsWith, findSplit;
		import std.conv : ConvException;
		import std.getopt : GetOptException;
		auto arg = args.front;
		args.popFront();

		if (!arg.startsWith("--"))
		{
			break;
		}
		auto pieces = arg[2..$].findSplit("=");
		auto arg_name = pieces[0];
		auto arg_value = pieces[2];

		static bool boolValue(string v)
		{
			import std.string : toLower;
			switch (v.toLower)
			{
				case "":
				case "y":
				case "yes":
				case "true":
					return true;

				case "n":
				case "no":
				case "false":
					return false;

				default:
					throw new ConvException("Couldn't interpret '" ~ v ~ "'.  'yes' or 'no' required");
			}
		}

		try
		{
			switch (arg_name)
			{
				case "test":
					config.test = boolValue(arg_value);
					break;

				case "fullscreen":
					config.fullscreen = boolValue(arg_value);
					break;

				default:
					throw new GetOptException("Unrecognised argument: '" ~ arg_name ~ "'.  (Use the source for help :)");
			}
		}
		catch (Exception e)
		{
			io.stderr.writeln("Error processing argument '" ~ arg_name ~ "': " ~ e.msg);
			return 1;
		}
	}

	if (config.test) return 0;

	try
	{
		enforce(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS | SDL_INIT_AUDIO) == 0, "Failed to start SDL");
		if (config.fullscreen)
		{
			enforce(SDL_CreateWindowAndRenderer(0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP, &window, &renderer) == 0, "Failed to create fullscreen SDL window and renderer");
		}
		else
		{
			enforce(SDL_CreateWindowAndRenderer(640, 400, 0, &window, &renderer) == 0, "Failed to create SDL window and renderer");
		}
		enforce(window, "Failed to create window");
		enforce(renderer, "Failed to create renderer");

		enforce(SDL_ShowCursor(SDL_DISABLE) >= 0, "Failed to hide cursor");

		enforce(SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"), "Failed to set scaling hint");
		enforce(SDL_RenderSetLogicalSize(renderer, kScreenWidth, kScreenHeight) == 0, "Failed to set logical screen size");
		texture = SDL_CreateTexture(renderer, kTextureFormat, SDL_TEXTUREACCESS_STREAMING, kScreenWidth, kScreenHeight);
		enforce(texture, "Failed to create SDL texture");
		surface8 = SDL_CreateRGBSurface(0, kScreenWidth, kScreenHeight, 8, 0, 0, 0, 0);
		enforce(surface8, "Failed to create SDL surface8");
		surface32 = SDL_CreateRGBSurface(0, kScreenWidth, kScreenHeight, 32, 0, 0, 0, 0);
		enforce(surface32, "Failed to create SDL surface32");

		SDL_AddTimer(20, &timerCallback, null);
	}
	catch (Exception e)
	{
		io.stderr.writeln(e.msg);
		showSdlError();
		return 2;
	}

	atexit(&cleanup);

	osMain();
	return 0;
}

extern(C) void cleanup() @nogc nothrow
{
	SDL_CloseAudio();
	SDL_DestroyTexture(texture);
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}

extern(C) uint timerCallback(uint interval, void* unused) @nogc nothrow
{
	SDL_Event event = {type: SDL_USEREVENT};
	SDL_PushEvent(&event);
	return interval;
}

void showSdlError() @nogc nothrow
{
	import core.stdc.stdio : fputs, stderr;
	auto message = SDL_GetError();
	fputs("SDL Error: ", stderr);
	fputs(message, stderr);
	fputs("\n", stderr);
}
