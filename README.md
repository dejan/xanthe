# Xanthe: Bare Metal D Vertical Shooter

This is a simple vertical shooter game written as a test of bare metal [D](https://dlang.org/).  It does not depend on the D runtime, or even `libc`, and can be booted directly on an x86 PC.  There's also an [SDL](https://www.libsdl.org/)-based build for testing on a desktop.

## Demo

You can watch [a short demo video of the game on Youtube](http://www.youtube.com/watch?v=bHFsuS7xbV8).

## Requirements

* A [D compiler](https://dlang.org/download.html)
* GNU development environment (`make`, `ld`, etc)

For bare metal backend:
* NASM
* Qemu (or Bochs) if booting in a virtual machine
* GRUB bootloader (optional, but recommended if you want to boot on real hardware)
* `xorriso` (from `libburn`) if building a bootable ISO image

For SDL backend:
* Core SDL2 library

## `make` Targets

* `play`: build and play the SDL version
* `test`: build and run tests
* `qemu-mb`: build and run the multiboot version in Qemu
* `qemu-bios`: build and run the BIOS version in Qemu
* `mb`: build a multiboot-compatible kernel (`mb-build/xanthe`) than can be booted using GRUB or other bootloaders
* `xanthe.iso`: build a GRUB-based bootable ISO image (that can also be written to a USB or hard drive)

See the `Makefile` for other targets and some variables you might need to tweak.

## Platforms that Work

The SDL version should run on any x86 system that supports SDL.

The bare x86 version requires at least a Pentium (though it hasn't been tested on an actual Pentium).  The Sound Blaster 16 is currently the only sound card supported.  Unfortunately, [the BIOS bootloader](https://gitlab.com/sarneaud/hello-d-x86-bios) isn't very portable.  It works on SeaBIOS, used by Qemu and Bochs.  The multiboot build is recommended for other platforms.

Xanthe was developed on a GNU/Linux system, and there might be some hiccups building it on other OSes.

## Media

The graphics are mostly 8-bit PCX sprites drawn using Aseprite.

The explosion sound effects are from [a set produced by Victor Hahn](http://opengameart.org/content/9-explosion-sounds).  The music and other sound effects were synthesised for this project using the [ChucK audio programming language](http://chuck.cs.princeton.edu/).  All the music is based on the score of Rachmaninoff's Piano Concerto No.2, Op 18.

## Known Issues

* The BIOS bootloader isn't very portable.  The multiboot build works with GRUB, which is much more reliable.
* Audio only works either in the SDL build, or on hardware with a Sound Blaster 16
* `dmd` (tested with 2.071) puts two copies of all media into the binary for some reason, making the image about twice as big as needed
* The BIOS boot takes several seconds

Note that this is highly experimental work and contains some hacks.  I really can't guarantee it'll work for you.
