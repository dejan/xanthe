Audio files are 16-bit signed mono PCM at 44100Hz.

If you have sox installed, you can play them like this:

play -e signed -r 44100 -c 1 -b 16 moderato.raw
